// Copyright Red Energy Limited 2017

package simplenem12;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

class SimpleNem12ParserImpl implements SimpleNem12Parser {
    
    private static final String RECORD_TYPE_FIRST = "100";
    private static final String RECORD_TYPE_START = "200";
    private static final String RECORD_TYPE_READ = "300";
    private static final String RECORD_TYPE_LAST = "900";
    
    public Collection<MeterRead> parseSimpleNem12(File simpleNem12File) {
        
        if(!simpleNem12File.exists()){
            return null;
        }

        List<MeterRead> meterReadList = new ArrayList<>();
        try(BufferedReader br = Files.newBufferedReader(simpleNem12File.toPath())){

            Optional<String> firstLine = br.lines().findFirst();
            List<String> meterRecordLines = br.lines().map(String::trim).collect(Collectors.toList());

            if (checkStartAndEndLines(firstLine, meterRecordLines)) return meterReadList;

            meterRecordLines.stream().filter(line -> line.startsWith(RECORD_TYPE_START) || line.startsWith(RECORD_TYPE_READ)).forEach(line -> {
                        if(line.startsWith(RECORD_TYPE_START)) {
                            readRecord200(line).ifPresent(meterReadList::add);
                        }
                        if(line.startsWith(RECORD_TYPE_READ) && (meterReadList.size() >= 1)) {
                            readRecord300(meterReadList.get(meterReadList.size()-1), line);
                        }
                    });
        } catch (IOException io) {
            io.printStackTrace();
        }
        return meterReadList;
    }

    private boolean checkStartAndEndLines(Optional<String> firstLine, List<String> lines) {
        if(!firstLine.isPresent() ||
                !firstLine.get().trim().equals(RECORD_TYPE_FIRST) ||
                !lines.get(lines.size()-1).equals(RECORD_TYPE_LAST)) {
            return true;
        }
        return false;
    }

    private Optional<MeterRead> readRecord200(String record200) {
        if (record200 != null && !record200.isEmpty()) {
            String[] items = record200.split(",");
            if(items.length == 3) {
                return Optional.of(new MeterRead(items[1], EnergyUnit.valueOf(items[2])));
            } else {
                return Optional.empty();
            }
        } else {
            return Optional.empty();
        }
    }

    private void readRecord300(MeterRead meterRead, String record300) {
        if (record300 != null && !record300.isEmpty()) {
            String[] items = record300.split(",");
            if(items.length == 4) {
                MeterVolume meterVolume = new MeterVolume(new BigDecimal(items[2]), Quality.valueOf(items[3]));
                meterRead.getVolumes().put(LocalDate.parse(items[1], DateTimeFormatter.ofPattern("yyyyMMdd")), meterVolume);
            }
        }
    }

}
